import  ballerina/http;
import  ballerina/url;
import  ballerina/lang.'string;

public type ClientConfig record {
    http:CredentialsConfig authConfig;
    http:ClientSecureSocket secureSocketConfig?;
};

public type CourseOutlineArr CourseOutline[];

# This API is used to enable users to gain access to a courseoutline.
#
# + clientEp - Connector http endpoint
public client class Client {
    http:Client clientEp;
    public isolated function init(ClientConfig clientConfig, string serviceUrl = "http://localhost:9090/v1") returns error? {
        http:ClientSecureSocket? secureSocketConfig = clientConfig?.secureSocketConfig;
        http:Client httpEp = check new (serviceUrl, { auth: clientConfig.authConfig, secureSocket: secureSocketConfig });
        self.clientEp = httpEp;
    }
    #
    # + return - Course-outline avaliable to view
    remote isolated function  VIEW(string uSERNAME, string[]? cOURSEID = ()) returns CourseOutlineArr|error {
        string  path = string `/SERVICE/VIEW/`;
        map<anydata> queryParam = {"USERNAME": uSERNAME, "COURSEID": cOURSEID};
        path = path + check getPathForQueryParam(queryParam);
        http:Request request = new;
        //TODO: Update the request as needed;
        CourseOutlineArr response = check self.clientEp-> post(path, request, targetType = CourseOutlineArr);
        return response;
    }
    #
    # + return - Course-outline signed
    remote isolated function  APPROVAL(string uSERNAME, string mODULEID) returns error? {
        string  path = string `/SERVICE/APPROVAL/`;
        map<anydata> queryParam = {"USERNAME": uSERNAME, "MODULEID": mODULEID};
        path = path + check getPathForQueryParam(queryParam);
        http:Request request = new;
        //TODO: Update the request as needed;
         _ = check self.clientEp-> post(path, request, targetType =http:Response);
    }
    #
    # + return - Course-outline successfully create
    remote isolated function  CREATE(string uSERNAME, string mODULEID) returns error? {
        string  path = string `/SERVICE/CREATE/`;
        map<anydata> queryParam = {"USERNAME": uSERNAME, "MODULEID": mODULEID};
        path = path + check getPathForQueryParam(queryParam);
        http:Request request = new;
        //TODO: Update the request as needed;
         _ = check self.clientEp-> post(path, request, targetType =http:Response);
    }
    #
    # + return - Course-outline successfully updated
    remote isolated function  UPDATE(string uSERNAME, CourseOutline payload) returns error? {
        string  path = string `/SERVICE/UPDATE/`;
        map<anydata> queryParam = {"USERNAME": uSERNAME};
        path = path + check getPathForQueryParam(queryParam);
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
         _ = check self.clientEp-> post(path, request, targetType=http:Response);
    }
}

# Generate query path with query parameter.
#
# + queryParam - Query parameter map
# + return - Returns generated Path or error at failure of client initialization
isolated function  getPathForQueryParam(map<anydata>   queryParam)  returns  string|error {
    string[] param = [];
    param[param.length()] = "?";
    foreach  var [key, value] in  queryParam.entries() {
        if  value  is  () {
            _ = queryParam.remove(key);
        } else {
            if  string:startsWith( key, "'") {
                 param[param.length()] = string:substring(key, 1, key.length());
            } else {
                param[param.length()] = key;
            }
            param[param.length()] = "=";
            if  value  is  string {
                string updateV =  check url:encode(value, "UTF-8");
                param[param.length()] = updateV;
            } else {
                param[param.length()] = value.toString();
            }
            param[param.length()] = "&";
        }
    }
    _ = param.remove(param.length()-1);
    if  param.length() ==  1 {
        _ = param.remove(0);
    }
    string restOfPath = string:'join("", ...param);
    return restOfPath;
}
