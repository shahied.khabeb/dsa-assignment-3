import  ballerina/http;

listener  http:Listener  ep0  = new (9090, config  = {host: "localhost"});

 service  /v1  on  ep0  {
        resource  function  post  SERVICE/VIEW(string  USERNAME, string[]?  COURSEID)  returns  'COURSE\-OUTLINE[]|record  {|*http:Unauthorized; record  {|string  message;|}  body;|}|record  {|*http:NotFound; record  {|string  message;|}  body;|} {
    }
        resource  function  post  SERVICE/APPROVAL(string  USERNAME, string  MODULEID)  returns  http:Ok|record  {|*http:Unauthorized; record  {|string  message;|}  body;|}|record  {|*http:NotFound; record  {|string  message;|}  body;|} {
    }
        resource  function  post  SERVICE/CREATE(string  USERNAME, string  MODULEID)  returns  http:Ok|record  {|*http:Unauthorized; record  {|string  message;|}  body;|}|record  {|*http:NotFound; record  {|string  message;|}  body;|} {
    }
        resource  function  post  SERVICE/UPDATE(string  USERNAME, @http:Payload  {} 'COURSE\-OUTLINE  payload)  returns  http:Ok|record  {|*http:Unauthorized; record  {|string  message;|}  body;|}|record  {|*http:NotFound; record  {|string  message;|}  body;|} {
    }
}
