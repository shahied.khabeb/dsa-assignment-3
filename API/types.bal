public type CourseContent record {
    string ID;
    string Name?;
    CourseContent[] Chapter?;
};

public type Lecture record {
    string ID;
    string NAME;
    string 'Lectures\-Email?;
    string 'Lectures\-Cell?;
    string 'Lectures\-Location?;
};

public type ASSESSMENT record {
    string ID;
    string NAME;
    # The tests weigh 25% each
    int weight?;
};

public type CourseOutline record {
    string ID;
    string NAME;
    string OUTCOMES;
    string DEPARTMENT?;
    string PROGRAMME?;
    int NQFLEVEL?;
    int NQFCREDITS?;
    string[] PREREQUISITES?;
    Lecture[] LECTURES?;
    string HoD?;
    string[] LOUTCOMES?;
    CourseOutline[] CHAPTERS?;
    # The calender shows the dues and the time that tests and other assements are due
    string[] 'COURSE\-CALENDAR?;
};
