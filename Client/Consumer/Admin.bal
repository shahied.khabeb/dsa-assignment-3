
import ballerina/test;
import ballerina/http;


type Payload record {|
    string Username;
    string Password;
 
|};


http:Client httpClient = new http:Client("http://localhost:9090/");


@test:Config {}
function testProductAdminPortal () {
   
    http:Request req = new();

    
    Payload payload = { Username:"Admin", Password:"Admin",};
    json|error payloadJson = typedesc<json>.constructFrom(payload);

    if (payloadJson is error) {
        test:assertFail(msg = "Payload JSON returned error.");
    } else {

        req.setJsonPayload(payloadJson);
        // Send a 'post' request and obtain the response
        http:Response|error postResponse = httpClient->post("/", req);

        if (postResponse is error) {
            test:assertFail(msg = "HTTP post method returned error.");
        } else {
            // Expected response code is 200
            test:assertEquals(postResponse.statusCode, 200,
                    msg = "product admin service did not respond with 200 OK signal!"
            );
            // Check whether the response is as expected
            var resPayload = postResponse.getJsonPayload();
            if (resPayload is error) {
                test:assertFail(msg = "Response payload returned error.");
            } else {
                json expected = {"Status":"Success"};
                test:assertEquals(resPayload, expected, msg = "Response mismatch!");
            }
        }
    }
}