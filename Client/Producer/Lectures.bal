
import ballerina/io;
import ballerina/kafka;
import ballerina/encoding;

// Kafka consumer listener configurations
kafka:ConsumerConfig consumerConfig = {
    bootstrapServers: "localhost:9093",
    // Consumer group ID
    groupId: "Lectures-group",
    // Listen from topic 'product-price'
    topics: ["Lectures-service"],
    // Poll every 1 second
    pollingIntervalInMillis: 1000
};

// Create kafka listener
listener kafka:Consumer consumer = new(consumerConfig);

// Kafka service that listens from the topic 'product-price'
// 'FranchiseeService2' subscribed to new product price updates from the product admin
service lecturesproducer on consumer {
    // Triggered whenever a message added to the subscribed topic
    resource function onMessage(kafka:Consumer simpleConsumer, kafka:ConsumerRecord[] records) {
        // Dispatched set of Kafka records to service, We process each one by one.
        foreach var entry in records {
            byte[] serializedMsg = entry.value;
            // Convert the serialized message to string message
            string msg = encoding:byteArrayToString(serializedMsg);
            io:println("[INFO] New message received from the admin");
            // log the retrieved Kafka record
            io:println("[INFO] Topic: " + entry.topic + "; Received Message: " + msg);
            // Acknowledgement
            io:println("[INFO] Acknowledgement from Hod");
        }
    }
}