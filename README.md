# DSA Assignment 3
This assigment was done mutually by :
liandro Goagoseb 220059861  
Richard !Khabeb  220026262




## Automated Courseoutline
The following are the sections available in this guide.
1. Prerequisites 
2. design this application following a micro-service architectural style;
3. Set up a Kafka cluster or instance for the communication;
4. Implement the service(s);
5. deploy the services using Kubernetes orchestrator.


## Prerequisites 

Kafka - 2.13-3.0.0

Ballerina - Swan Lake Beta 3

Kubernetes

## Structure
```
C:.
│   README.md
│
├───API
│       client.bal
│       Course-outline.yml        
│       course-outline_service.bal
│       types.bal
│
├───Client
│   ├───Consumer
│   │       Admin.bal
│   │       Main.bal
│   │
│   ├───Producer
│   │       HoD.bal
│   │       Lectures.bal
│   │       Students.bal
│   │
│   └───YAML
│           HoD.yml
│           Lecture.yml
│           Students.yml
│
└───Service
    ├───HoD
    │       microservice_1.bal
    │       service-config.toml
    │
    ├───Lectures
    │       microservice_2.bal
    │       
    └───Students
            microservice_3

```

## Design this application following a micro-service architectural style/implementation of service;
Microservice architecture, or simply microservices, is a distinctive method of developing software systems that tries to focus on building single-function modules with well-defined interfaces and operations.


We Broken done the services into the multiple microservices:


The “HOD” microservice

Language: Kubernetes/ballerina


Description:
input information about a course, a lecturer and a student;
view all generated course outlines;
(digitally) approve a course outline



The “Student” microservice


Language: Kubernetes/ballerina

Description: 

view a course outline generated for a course he/she is taking;
acknowledge reception of a generated course outline for a course he/she is taking.


The “Lecturer” microservice

Language: Kubernetes/ballerina

Description:


input information about a course he/she has been assigned;
view course outlines he/she has generated;
generate and sign a course outline for a course he/she has been assigned

## Set up a Kafka cluster or instance for the communication
#Kafka files
Consumer

-Admin

Producer

-HOD

-STUDENT

-Lectuer

#Kafka Topics
```
Courseoutline -   Used to send file to HOD,Student,Lecturer

Lectures-service - Used to send file to HOD,Student,Lecturer

student-service -  Used to send file to HOD,Student,Lecturer
```

#Kafka Configurations

HOD
```
broker.id=1
Port:9092

```
Student
```
broker.id=2
Port:9093

```
Lecturer
```
broker.id=3
Port:9094

```


## deploy the services using Kubernetes orchestrator.
HOD
```
Compiling source
  Microservice_1.bal

Generating executable
    Microservice_1.jar

Generating artifacts...

	@kubernetes:Service 			 - complete 1/1
	@kubernetes:Deployment 			 - complete 1/1
	@kubernetes:Docker 			 - complete 2/2
	@kubernetes:Helm 			 - complete 1/1

	Execute the below command to deploy the Kubernetes artifacts:
	kubectl apply -f ./kubernetes

	Execute the below command to install the application using Helm:
	helm install --name hello-world-k8s-deployment ./ Microservice_1-deployment

```
Student
```
Compiling source
  Microservice_2.bal

Generating executable
    Microservice_2.jar

Generating artifacts...

	@kubernetes:Service 			 - complete 1/1
	@kubernetes:Deployment 			 - complete 1/1
	@kubernetes:Docker 			 - complete 2/2
	@kubernetes:Helm 			 - complete 1/1

	Execute the below command to deploy the Kubernetes artifacts:
	kubectl apply -f ./kubernetes

	Execute the below command to install the application using Helm:
	helm install --name  Microservice_2-deployment ./ Microservice_2-deployment

```
Lecturer
```

Compiling source
  Microservice_3.bal

Generating executable
    Microservice_3.jar

Generating artifacts...

	@kubernetes:Service 			 - complete 1/1
	@kubernetes:Deployment 			 - complete 1/1
	@kubernetes:Docker 			 - complete 2/2
	@kubernetes:Helm 			 - complete 1/1

	Execute the below command to deploy the Kubernetes artifacts:
	kubectl apply -f ./kubernetes

	Execute the below command to install the application using Helm:
	helm install --name Microservice_3-deployment ./kubernetes/Microservice_3-deployment

```

