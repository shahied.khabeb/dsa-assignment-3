import ballerina/http;
import ballerinax/kubernetes;



@kubernetes:IstioGateway {
}
@kubernetes:IstioVirtualService {}
@kubernetes:Service {
name: "Primary service",
}
serviceType: "NodePort"
endpoint http:Listener Primary-serviceEP {
port: 9080
};
endpoint http:Client microservice_2EP {
url: "http://Student-view-service:8080"
};
endpoint http:Client microservice_3EP {
url: "http://Lectures-service:7070"
};
@kubernetes:Deployment {
name: "Main service"
}

@kubernetes:Secret{

}
name: "Authorization"

@http:ServiceConfig {
basePath: "/"
}
service<http:Service> shopService bind Primary-serviceEP {
@http:ResourceConfig {
methods: ["GET"),
path: "/{id}"
}
getCourseoutline (endpoint caller, http:Request request, string id) {
var detailCall = microservice_2EP->get (string / Course / string/ LectureInformation/string/ LearnOutComes/string/ CourseContent/string Assessment/string Appproved /{{untaint id}});
match detailCall {
http:Response detailResponse => {
if (detailResponse.statusCode != 404) {
json details = check detailResponse.getJsonPayload ();
var reviewCall = microservice_3EP->get (string / Course / string/ LectureInformation/string/ LearnOutComes/string/ CourseContent/string Assessment/string Appproved /{{untaint id}});
match reviewCall {
http:Response reviewResponse => {
string reviews = check reviewResponse.getTextPayload ();
json CourseOutline = {
id: id,
Course: details,
LectureInformation: reviews,
LearnOutComes: learnoutcome,
CourseContent:courseinfor,;
Assessment:studentgrades,;
Appproved:coursepermit;

};
http:Response viewResponse = new;

http:Response viewResponse
viewResponse. setJsonPayload (untaint Courseoutline, contentType = "application/json");
= caller -> respond (viewResponse);
}
error => {
http:Response errResponse new;
json serverErr = { message: "Courseoutline is not accessible" };
errResponse.setJsonPay load (serverErr, contentType "application/json");
- = caller -> respond (errResponse);
}

}
else {
http:Response errResponse = new;
json serverErr = { message: string book not found: {{untaint id}} };
errResponse. setJsonPayload(serverErr, contentType = "application/json");
errResponse.statusCode = 404;
= caller -> respond (errResponse);
}
error => {
http:Response errResponse = new;
json serverErr = { message: "Courseoutline is not accessible" };