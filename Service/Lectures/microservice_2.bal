import ballerina/http;
import ballerinax/kubernetes;



@kubernetes:Service {
name: "Lectures-service",
serviceType: "ClusterIP"
}

endpoint http:Listener  microservice_3EP {
port: 7070
};

@kubernetes:Deployment {
name: "Lectures-service"
}


@http:ServiceConfig {
basePath: "/review"
}
service<http:Service> reviewService bind  microservice_3EP {
@http:ResourceConfig {
methods: ["POST"),
path: "/{id)}"
createcourseoutline (endpoint caller, http:Request request, string id) {
table<view> tbReviews = table {
{
id, Course,LectureInformation,LearnOutComes,CourseContent,Assessment,Appproved },
[ { "C1", "Web Application development,Name: Simon H. Muchinenyika Lecturer’s Email:smuchinenyika@nust.na Cell: 2072054 ,DEPARTMENT:Computer Science PROGRAMME: Bachelor of Computer Science: Software Development,Test Quizzes and Assigments,Signed," },

};
string reviewContent = " (no courseoutline found)";
while (tbReviews.hasNext()) {
view review = check <Review>tbReviews.getNext ();

if (review.id == id) {
reviewContent = review.content;
break;
}
}