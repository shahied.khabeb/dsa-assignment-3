import ballerina/http;
import ballerinax/kubernetes;


@kubernetes:Service {
name:"Student-view-service",
serviceType: "ClusterIP"
}

endpoint http:Listener microservice_2EP  {
port: 8080
};

@kubernetes: Deployment {
name:"Student-view-service"
}

@http:ServiceConfig {
basePath: "/detail"
}
service<http:Service> detailService bind microservice_2EP {
@http:ResourceConfig {
methods: ["PUT"),
path: "/{id)}"
updatecourseoutline (endpoint caller, http:Request request, string id) {
table<Approve> tbDetails = table {
{
{id,Approve},
{ "B1", "not approve" },
{ "B2", "approve" },
{ "B3", "pending approval"}
};
Approve? Coursedetails;
while (tbDetails.hasNext () {
Approve detail = check <Detail>tbDetails.getNext();
if (detail.id == id) {
Coursedetails = detail;
break;

http:Response detailResponse = new;
match Coursedetails {
Approve detail => {
json responseJson = {
Appprove: Coursdetails.cost
};
detailResponse.setJsonPayload (responseJson, contentType = "application/json");
}
() => {
json notFoundJson = {
message: "Courseoutline not approved: " + untaint id
};
detailResponse.setJsonPayload (notFoundJson, contentType = "application/json");
detailResponse.statusCode = 404;
}
= caller -> respond (detailResponse);
}
}
